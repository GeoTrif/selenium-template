package com.template.service;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static com.template.util.Constants.*;

public class AutomatedFormCompletionService {

    public void automateFormCompletion(WebDriver driver) {
        driver.navigate().to(LOCALHOST_URL);
        loginAutomation(driver);
    }

    private void loginAutomation(WebDriver driver) {
        WebElement user = driver.findElement(By.id(USERNAME_ID));
        user.sendKeys(USER_KEY);

        WebElement password = driver.findElement(By.id(PASSWORD_ID));
        password.sendKeys(PASSWORD_KEY);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement submit = driver.findElement(By.cssSelector(BEAM_SUBMIT_SELECTOR));
        submit.click();

        automateBeamDesignForm(driver);
    }

    private void automateBeamDesignForm(WebDriver driver) {
        WebElement beamDesignNavItem = driver.findElement(By.cssSelector(BEAM_NAV_BAR_SELECTOR));
        beamDesignNavItem.click();

        Select concrete = new Select(driver.findElement(By.name(CONCRETE_NAME_TAG)));
        concrete.selectByVisibleText(CONCRETE_DROPDOWN_CHOICE);
        Select longBarsSteel = new Select(driver.findElement(By.name(LONG_BARS_STEEL_NAME_TAG)));
        longBarsSteel.selectByVisibleText(LONG_BARS_STEEL_DROPDOWN_CHOICE);
        Select stirrupsSteel = new Select(driver.findElement(By.name(STIRRUPS_STEEL_NAME_TAG)));
        stirrupsSteel.selectByVisibleText(STIRRUPS_STEEL_DROPDOWN_CHOICE);

        WebElement beamLength = driver.findElement(By.name(BEAM_LENGTH_NAME_TAG));
        beamLength.clear();
        beamLength.sendKeys(BEAM_LENGTH_KEY);

        WebElement slabThickness = driver.findElement(By.name(SLAB_THICKNESS_NAME_TAG));
        slabThickness.clear();
        slabThickness.sendKeys(SLAB_THICKNESS_KEY);

        // The rest of the form fields....
        // Similar to beamLength element.
        WebElement beamDesignSubmitButton = driver.findElement(By.name(BEAM_DESIGN_SUBMIT_NAME_TAG));
        beamDesignSubmitButton.click();
    }
}
