package com.template.service;

import static com.template.util.Constants.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class BasicSeleniumCommandsService {

    public void navigateToAPageAndGetTitle(WebDriver driver) {
        driver.get(EMAG_URL);
        String appTitle = driver.getTitle();

        System.out.println("Title is : " + appTitle);
    }

    public void getTagNameValueFromWebElement(WebDriver driver) {
        String tagNameValue = "";

        driver.get(PERSONAL_SITE_URL);

        tagNameValue = driver.findElement(By.tagName(H2_TAG)).getText();
        System.out.println(tagNameValue);
    }

    public void navigateCommands(WebDriver driver) {
        // Equivalent to driver.get() method,but keeps cookies and history.
        driver.navigate().to(PERSONAL_SITE_URL);

        // For CSS Selector, inspect the element you want to find,right click on it in inspect mode,copy, cssSelector.
        driver.findElement(By.cssSelector(PROJECTS_SELECTOR)).click();

        driver.navigate().refresh();
        driver.navigate().back();
        driver.navigate().forward();
        driver.close();
    }

    public void moveMouseOverElements(WebDriver driver) {
        driver.navigate().to(GURU_NEW_TOURS_URL);

        WebElement signOn = driver.findElement(By.cssSelector(SIGN_ON_SELECTOR));
        WebElement home = driver.findElement(By.cssSelector(HOME_SELECTOR));

        Actions actions = new Actions(driver);
        Action moveMouseOverElements = actions.moveToElement(home).build();
        moveMouseOverElements.perform();

    }

    public void dragAndDropElements(WebDriver driver) {
        driver.navigate().to(GURU_DRAG_DROP_URL);

        WebElement dragElement = driver.findElement(By.xpath(DRAG_ELEMENT_XPATH));
        WebElement dropElement = driver.findElement(By.xpath(DROP_ELEMENT_XPATH));

        Actions actions = new Actions(driver);
        actions.dragAndDrop(dragElement, dropElement).build().perform();
    }

    public void play1024(WebDriver driver) {
        driver.navigate().to(GAME_1024_URL);

        WebElement gameBoard = driver.findElement(By.cssSelector(GAME_BOARD_SELECTOR));

        Actions actions = new Actions(driver);

        while (true) {
            actions.sendKeys(Keys.UP, Keys.DOWN, Keys.LEFT, Keys.RIGHT).perform();
        }

    }
}
