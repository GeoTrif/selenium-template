package com.template.application;

import com.template.service.AutomatedFormCompletionService;
import com.template.service.BasicSeleniumCommandsService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumTemplateApplication {

    private static final AutomatedFormCompletionService AUTOMATED_FORM_COMPLETION_SERVICE = new AutomatedFormCompletionService();
    private static final BasicSeleniumCommandsService BASIC_SELENIUM_COMMANDS_SERVICE = new BasicSeleniumCommandsService();

    private static final String WEBDRIVER_NAME = "webdriver.gecko.driver";
    private static final String WEBDRIVER_FILE_PATH = "geckodriver.exe";

    public static void main(String[] args) {

        // Check here for Selenium Tutorials : https://www.guru99.com/selenium-tutorial.html
        // Also check for the latest drivers for the WebDriver.Here I'm using Gecko Driver for Firefox.
        System.setProperty(WEBDRIVER_NAME, WEBDRIVER_FILE_PATH);
        WebDriver driver = new FirefoxDriver();

        AUTOMATED_FORM_COMPLETION_SERVICE.automateFormCompletion(driver);
        BASIC_SELENIUM_COMMANDS_SERVICE.dragAndDropElements(driver);
    }
}
