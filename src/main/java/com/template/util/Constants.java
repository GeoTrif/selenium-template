package com.template.util;

public class Constants {

    // BasicSeleniumCommandsService
    public static final String H2_TAG = "h2";
    public static final String EMAG_URL = "https://www.emag.ro/";
    public static final String PROJECTS_SELECTOR = "#hy5jsc6d1label";
    public static final String GAME_1024_URL = "https://1024game.org/";
    public static final String DROP_ELEMENT_XPATH = "//*[@id=\"bank\"]/li";
    public static final String DRAG_ELEMENT_XPATH = "//*[@id=\"credit2\"]/a";
    public static final String PERSONAL_SITE_URL = "https://geotrif.wixsite.com/projects";
    public static final String GURU_NEW_TOURS_URL = "http://demo.guru99.com/test/newtours/";
    public static final String GURU_DRAG_DROP_URL = "http://demo.guru99.com/test/drag_drop.html";
    public static final String GAME_BOARD_SELECTOR = "body > div.columns > div.column.is-6 > div.game";
    public static final String SIGN_ON_SELECTOR = "body > div:nth-child(5) > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > a";
    public static final String HOME_SELECTOR = "body > div:nth-child(5) > table > tbody > tr > td:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > font > a";

    // AutomatedFormCompletionService
    public static final String USER_KEY = "user";
    public static final String BEAM_LENGTH_KEY = "5";
    public static final String PASSWORD_KEY = "12345";
    public static final String PASSWORD_ID = "password";
    public static final String USERNAME_ID = "username";
    public static final String SLAB_THICKNESS_KEY = "0.13";
    public static final String CONCRETE_NAME_TAG = "concrete";
    public static final String BEAM_LENGTH_NAME_TAG = "beamLength";
    public static final String CONCRETE_DROPDOWN_CHOICE = "C30/37";
    public static final String BEAM_DESIGN_SUBMIT_NAME_TAG = "Compute";
    public static final String LOCALHOST_URL = "http://localhost:8080/";
    public static final String SLAB_THICKNESS_NAME_TAG = "slabThickness";
    public static final String STIRRUPS_STEEL_NAME_TAG = "stirrupsSteel";
    public static final String LONG_BARS_STEEL_NAME_TAG = "longBarsSteel";
    public static final String STIRRUPS_STEEL_DROPDOWN_CHOICE = "BST500S";
    public static final String LONG_BARS_STEEL_DROPDOWN_CHOICE = "BST500S";
    public static final String BEAM_NAV_BAR_SELECTOR = "li.nav-item:nth-child(1) > a:nth-child(1)";
    public static final String BEAM_SUBMIT_SELECTOR = "body > div > section.login > div > div > div > div.card-body > form > input";
}
